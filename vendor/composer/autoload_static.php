<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit79499578c79b0b224db99051d2b6c3af
{
    public static $prefixLengthsPsr4 = array (
        'c' => 
        array (
            'chriskacerguis\\RestServer\\' => 26,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'chriskacerguis\\RestServer\\' => 
        array (
            0 => __DIR__ . '/..' . '/chriskacerguis/codeigniter-restserver/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit79499578c79b0b224db99051d2b6c3af::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit79499578c79b0b224db99051d2b6c3af::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit79499578c79b0b224db99051d2b6c3af::$classMap;

        }, null, ClassLoader::class);
    }
}
