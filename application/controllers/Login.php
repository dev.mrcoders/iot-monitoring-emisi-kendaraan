<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model', 'Auth');
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function AuthChecking()
    {
        $FormData = [
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password'))
        ];

        $IsLogin = $this->Auth->GetAccount($FormData);

        if ($IsLogin) {
            $user_ses = [
                'user'  => $IsLogin->username,
                'login' => TRUE
            ];

            $this->session->set_userdata($user_ses);
            $Response = [
                'success' => true,
                'status' => "200",
                'message' => "Login Berhasil, Redirect To dashboard",
                'data' => [
                    'redirect' => 'dashboard'
                ]
            ];
        } else {
            $Response = [
                'success' => false,
                'status' => "404",
                'message' => "Login Gagal, Username Atau Password Salah",
                'data' => [
                    'redirect' => ''
                ]
            ];
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
    }

    public function logout()
    {
        $data_session = array('user' => "", 'login' => "");
        $this->session->unset_userdata($data_session); //clear session
        $this->session->sess_destroy(); //tutup session
        redirect(base_url());
    }
}

/* End of file Login.php and path \application\controllers\Login.php */
