<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['page'] = 'halaman/data';
        $this->load->view('layout/index', $data);
    }

    public function Data()
    {
        $this->db->join('tb_jenis_kendaraan b', 'a.id_kendaraan=b.id_kendaraan', 'inner');

        if ($this->input->get('id')) {
            $this->db->where('a.id_pengguna', $this->input->get('id'));
        }
        $this->db->order_by('id_pengguna', 'desc');

        $list = $this->db->get('tb_data_pengguna a');

        $result = ($this->input->get('id') ? $list->row() : $list->result());

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function DataUji()
    {
        $this->db->select('a.*,b.jenis_kendaraan,b.tipe_langkah');
        $this->db->select('(SELECT AVG(ppm) FROM tb_data_monitoring_emisi c WHERE c.id_pengguna=a.id_pengguna)as avg_ppm');
        $this->db->select('(SELECT AVG(co) FROM tb_data_monitoring_emisi c WHERE c.id_pengguna=a.id_pengguna)as avg_co');
        $this->db->join('tb_jenis_kendaraan b', 'a.id_kendaraan=b.id_kendaraan', 'inner');
        $this->db->order_by('id_pengguna', 'desc');

        $list = $this->db->get('tb_data_pengguna a');

        $result = $list->result_array();

        foreach ($result as $key_a => $value) {

            $stats = $this->statusUji($value['tahun_produksi'],$value['id_kendaraan'],round($value['avg_ppm'],2),round($value['avg_co'],2));
            $result[$key_a]['avg_ppm']=round($value['avg_ppm'],2);
            $result[$key_a]['avg_co']=round($value['avg_co'],2);
            $result[$key_a]['ppm_stat']=$stats['ppm'];
            $result[$key_a]['co_stat']=$stats['co'];
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    function statusUji($tprod,$idkend,$ppm,$co){
        $this->db->where('a.id_kendaraan', $idkend);
        $GetJenisKendaraan = $this->db->get('tb_jenis_kendaraan a')->row();
        if( $tprod < $GetJenisKendaraan->tahun_produksi){
            $logik = "<";
        }else if($tprod > $GetJenisKendaraan->tahun_produksi){
            $logik = ">";
        }

        $GetAmbang = $this->db->get_where('tb_ambang_batas_emisi',['id_jenis_kendaraan'=>$idkend,'deksripsi'=>$logik])->row();

        $stats_ppm = ($ppm > $GetAmbang->hc ? 'Berbahaya':'Aman');
        $stats_co = ($co > $GetAmbang->co ? 'Berbahaya':'Aman');

        return ['ppm'=>$stats_ppm,'co'=>$stats_co];


    }

    public function DataKendaraan()
    {
        if ($this->input->get('id_')) {
            $this->db->where('id_kendaraan', $this->input->get('id_'));
        }

        $list = $this->db->get('tb_jenis_kendaraan');

        $result = ($this->input->get('id_') ? $list->row() : $list->result());

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function SaveData()
    {
        $PostData = [
            'perusahaan' => $this->input->post('perusahaan'),
            'nama_pemilik' => $this->input->post('nama_pemilik'),
            'id_kendaraan' => $this->input->post('id_kendaraan'),
            'no_plat' => $this->input->post('no_plat'),
            'tahun_produksi' => $this->input->post('tahun_produksi'),
        ];

        if ($this->input->post('id_') == 0) {
            $this->db->insert('tb_data_pengguna', $PostData);

            $Response = [
                'success' => true,
                'message' => 'Data Berhasil Di Simpan'
            ];
        } else {
            $this->db->where('id_pengguna', $this->input->post('id_'));
            $this->db->update('tb_data_pengguna', $PostData);

            $Response = [
                'success' => true,
                'message' => 'Data Berhasil Di Ubah'
            ];
        }



        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
    }

    public function DeleteData()
    {
        $this->db->where('id_pengguna', $this->input->post('id'));
        $this->db->delete('tb_data_pengguna');

        $Response = [
            'success' => true,
            'message' => 'Data Berhasil Di Ubah'
        ];

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
    }

    public function view($id)
    {
        $this->db->join('tb_jenis_kendaraan b', 'a.id_kendaraan=b.id_kendaraan', 'inner');
        $data['data'] = $this->db->get_where('tb_data_pengguna a', ['a.id_pengguna' => $id])->row();
        $data['uji']=$this->db->get('tb_list_pengujian')->result();
        $data['page'] = 'halaman/data_view';
        $this->load->view('layout/index', $data);
    }

    public function SetActive()
    {
        $idPengguna = $this->input->get('id_');
        $isActive = $this->input->get('stats');

        $GetDataPengguna = $this->db->get_where('tb_data_pengguna', ['is_aktif' => 'Y'])->result();
        if ($GetDataPengguna) {
            foreach ($GetDataPengguna as $pg) {
                $setIDPengguna[] = [
                    'id_pengguna' => $pg->id_pengguna,
                    'is_aktif' => 'N'
                ];
            }
            $this->db->update_batch('tb_data_pengguna', $setIDPengguna, 'id_pengguna');

            $this->db->where('id_pengguna', $idPengguna);
            $this->db->update('tb_data_pengguna', ['is_aktif' => ($isActive == 'true' ? 'Y' : 'N')]);

            $Response = [
                'status' => true,
                'message' => 'Data Pengguna ' . ($isActive == 'true' ? 'Aktif' : 'Tidak Aktif'),
            ];
        } else {
            $setIDPengguna[] = [
                'id_pengguna' => $idPengguna,
                'is_aktif' => ($isActive == 'true' ? 'Y' : 'N')
            ];
            $this->db->update_batch('tb_data_pengguna', $setIDPengguna, 'id_pengguna');

            $Response = [
                'status' => false,
                'message' => 'Data Pengguna ' . ($isActive == 'true' ? 'Aktif' : 'Tidak Aktif'),
            ];
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
    }

    public function UpdatePengujian()
    {
        $PengujianAktif = $this->db->get_where('tb_list_pengujian', ['aktif' => 'Y'])->result();
        foreach ($PengujianAktif as $key => $value) {
            $UpdUji[]=[
                'id_pengujian'=>$value->id_pengujian,
                'aktif'=>"N"
            ];
        }

        $this->db->update_batch('tb_list_pengujian', $UpdUji, 'id_pengujian');

        $this->db->where('id_pengujian', $this->input->post('id'));
        $this->db->update('tb_list_pengujian', ['aktif'=>"Y"]);

        $Response = [
            'status' => true,
            'message' => "Data Uji Aktif",
        ];

        $this->output->set_content_type('application/json')->set_output(json_encode($Response));
    }
}

/* End of file Data.php and path \application\controllers\Data.php */