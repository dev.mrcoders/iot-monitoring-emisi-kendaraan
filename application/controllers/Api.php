<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function data_get()
    {
        $this->db->order_by('id_monitoring_emisi', 'desc');
        $GetDataMonitoring = $this->db->get_where('tb_data_monitoring_emisi', ['id_pengguna' => $this->get('id_')])->row();
        if ($GetDataMonitoring) {
            $this->response([
                'data' => ['ppm' => $GetDataMonitoring->ppm, 'co' => $GetDataMonitoring->co]
            ], 200);
        } else {
            $this->response([
                'data' => ['ppm' => 0, 'co' => 0]
            ], 200);
        }
    }

    public function data_post()
    {
        $id = $this->post('id_');
        $this->db->order_by('id_monitoring_emisi', 'desc');

        $GetData = $this->db->get_where('tb_data_monitoring_emisi', ['id_pengguna' => $id])->result();
        $this->response($GetData, 200);
    }

    public function chartppm_get()
    {
        $id = $this->get('id_');
        $this->db->order_by('id_monitoring_emisi', 'desc');

        $GetData = $this->db->get_where('tb_data_monitoring_emisi', ['id_pengguna' => $id])->row();
        $this->response($GetData, 200);
    }

    public function insert_get()
    {
        $GetAktifPengguna = $this->db->get_where('tb_data_pengguna', ['is_aktif' => "Y"])->row();
        $GetAktifPengujian = $this->db->get_where('tb_list_pengujian', ['aktif'=>"Y"])->row();
        if ($GetAktifPengguna) {

            $postData = [
                'id_pengguna' => $GetAktifPengguna->id_pengguna,
                'ppm' => $this->get('ppm'),
                'co' => $this->get('co'),
                'created' => date('Y-m-d H:i'),
                'id_pengujian'=>$GetAktifPengujian->id_pengujian
            ];
            if($this->get('ppm') > 100){
                $this->db->insert('tb_data_monitoring_emisi', $postData);
            }

            $response = [
                'message' => 'Success Saved',
            ];
            header("Access-Control-Allow-Origin: *");
            $this->response($response, 200);
        } else {
            $this->response(['message' => 'Data Not Aktif'], 200);
        }
    }
}

/* End of file Api.php and path \application\controllers\Api.php */