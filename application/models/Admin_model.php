<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Admin_model extends CI_Model 
{
    public function GetAccount($data)
    {
        return $this->db->get_where('tb_admin', $data,1)->row();
        
    }                        
                        
}


/* End of file Admin_model.php and path \application\models\Admin_model.php */
