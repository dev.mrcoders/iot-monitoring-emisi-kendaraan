<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">

            <p class="centered"><a href="profile.html"><img src="<?= base_url() ?>assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
            <h5 class="centered"><?=$this->session->userdata('user');?></h5>

            <li class="">
                <a class="<?= ($this->uri->segment(1) == 'dashboard' ? 'active' : '') ?> p-2" href="<?= base_url() ?>dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard </span>
                </a>
            </li>

            <li class="">
                <a class="<?= ($this->uri->segment(1) == 'data' ? 'active' : '') ?> p-2" href="<?= base_url() ?>data">
                    <i class="mdi mdi-database"></i>
                    <span>Data Monitoring</span>
                </a>
            </li>

            <!-- <li class="">
                <a class="<?= ($this->uri->segment(1) == 'pengaturan' ? 'active' : '') ?> p-2" href="<?= base_url() ?>pengaturan">
                    <i class="mdi mdi-database"></i>
                    <span>Pengaturan Ambang Batas</span>
                </a>
            </li> -->



        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>