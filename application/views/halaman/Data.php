<div class="row mt">
    <div class="col-md-12">
        <div class="content-panel">
            <div class="row">
                <div class="col-sm-6">
                    <h4>Data Monittoring</h4>
                </div>
                <div class="col-sm-6">
                    <button class="btn btn-sm btn-primary pull-right mr-2 btn-tambah">Tambah Data</button>
                </div>
            </div>

            <hr>
            <div class="table-responsive p-2">
                <table class="table table-striped table-advance table-hover" id="dt-table">
                    <thead>
                        <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2"> Nama Pemilik</th>
                            <th rowspan="2">Jenis Kendaraan</th>
                            <th rowspan="2">Tahun Produksi</th>
                            <th rowspan="2">No Plat</th>
                            <th colspan="2">Data Uji</th>
                            <th rowspan="2">Active</th>
                            <th rowspan="2"></th>
                        </tr>
                        <tr>
                            <th>PPM</th>
                            <th>CO %</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div> <!-- /content-panel -->
    </div><!-- /col-md-12 -->
</div>

<!-- Modal -->
<div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Data Pemilik Kendaraan Bermotor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-data">
                <input type="hidden" name="id_" value="0">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label for="exampleInputEmail1" class="col-form-label">Nama Pemilik</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="nama_pemilik" class="form-control " id="exampleInputEmail1" aria-describedby="emailHelp" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label for="exampleInputEmail1" class="col-form-label">Jenis Kendaraan</label>
                        </div>
                        <div class="col-sm-9">
                            <select name="id_kendaraan" id="" class="form-control">
                                <option value="">==Pilih Jenis Kendaran Tipe==</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label for="exampleInputEmail1" class="col-form-label">No Plat</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="no_plat" class="form-control " id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label for="exampleInputEmail1" class="col-form-label">Tahun Produksi</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="tahun_produksi" class="form-control " id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function() {

        LoadTables();
        $(document).ajaxComplete(function() {
            $('input[type=checkbox][data-toggle^=toggle]').bootstrapToggle({
                on: 'YES',
                off: 'NO',
                onstyle: 'success',
                offstyle: 'danger'
            });
        });

        function LoadTables() {
            $('#dt-table').DataTable({
                stateSave: true,
                destroy: true,
                ajax: {
                    url: base_url + "data/datauji",
                    type: "GET",
                    dataSrc: "",
                },
                columns: [{
                    render: function(data, type, row, meta) {
                        return meta.row + 1;
                    }
                },
                {
                    data: "nama_pemilik"
                },
                {
                    data: {
                        jenus_kendaraan: "jenis_kendaraan",
                        tipe_langkah: "tipe_langkar"
                    },
                    render: function(d) {
                        return d.jenis_kendaraan + ' - ' + d.tipe_langkah + ' Tak'
                    }
                },
                {
                    data: "tahun_produksi"
                },
                {
                    data: "no_plat"
                },
                {
                    data: {
                        avg_ppm:"avg_ppm",
                        ppm_stat:"ppm_stat",
                    },
                    render:function(data) {
                        return (data.avg_ppm == null ? '': parseFloat(data.avg_ppm).toFixed(2));
                    }
                },
                {
                    data: {
                        avg_co:"avg_co",
                        co_stat:"co_stat",
                    },
                    render:function(data) {
                        return (data.avg_co == null ? '': parseFloat(data.avg_co).toFixed(2));
                    }
                },
                {
                    data: {
                        is_aktif: 'is_aktif',
                        id_pengguna: 'id_pengguna'
                    },
                    render: function(data) {
                        let checked = (data.is_aktif == 'Y' ? 'checked' : '')
                        return '<input type="checkbox" ' + checked + ' data-toggle="toggle" data-size="xs" id="my-togle" class="is_active" data-id="' + data.id_pengguna + '">'
                    }
                },
                ],
                columnDefs: [{
                    targets: 8,
                    data: "id_pengguna",
                    render: function(data) {
                        return '<div class="btn-group btn-group-justified"><button class="btn btn-success btn-xs btn-view" data-id="' +
                        data + '"><i class="fa fa-eye"></i></button>' +
                        '<button class="btn btn-primary btn-xs btn-edit" data-id="' +
                        data + '"><i class="fa fa-pencil"></i></button>' +
                        '<button class="btn btn-danger btn-xs btn-hapus" data-id="' +
                        data + '"><i class="fa fa-trash-o "></i></button></div>';
                    }
                }]
            });
        }


        $('.btn-tambah').on('click', function() {
            $('#form-data')[0].reset();
            $('[name="id_"]').val('0');
            $('#add-modal').modal('show');
        })


        $('#dt-table').on('click', '.btn-view', function() {
            let id = $(this).data('id');
            window.location.href = base_url + "data/view/" + id;
        })

        $('#dt-table').on('change', '.is_active', function() {
            let id = $(this).data('id');
            let val = $(this).is(':checked');
            $.get(base_url + "data/setactive", {
                id_: id,
                stats: val
            },
            function(data, textStatus, jqXHR) {
                LoadTables();
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: data.message,
                    showConfirmButton: false,
                    timer: 1500
                })
            },
            "json"
            );
        })

        let opt = '';
        $.get(base_url + "data/datakendaraan",
            function(data, textStatus, jqXHR) {
                $.each(data, function(indexInArray, valueOfElement) {
                    opt += '<option value="' + valueOfElement.id_kendaraan + '">' + valueOfElement
                    .jenis_kendaraan + ' - ' +
                    valueOfElement
                    .tipe_langkah + ' TAK</option>'
                });
                $('[name="id_kendaraan"]').append(opt);
            },
            "json"
            );

        $('form#form-data').submit(function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            let DataForm = $(this).serialize();
            $.ajax({
                type: "POST",
                url: base_url + "data/saveData",
                data: DataForm,
                dataType: "JSON",
                success: function(response) {
                    LoadTables();
                    $('#add-modal').modal('hide');
                    $('#form-data')[0].reset();
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            });
        });

        $('#dt-table').on('click', '.btn-edit', function() {
            let id = $(this).data('id');
            $.ajax({
                type: "GET",
                url: base_url + "data/data",
                data: {
                    id: id
                },
                dataType: "JSON",
                success: function(response) {
                    $('#add-modal').modal('show');
                    $('[name="id_"]').val(id);
                    $('[name="perusahaan"]').val(response.perusahaan);
                    $('[name="nama_pemilik"]').val(response.nama_pemilik);
                    $('[name="id_kendaraan"]').val(response.id_kendaraan).trigger('change');
                    $('[name="no_plat"]').val(response.no_plat);
                    $('[name="tahun_produksi"]').val(response.tahun_produksi);
                }
            });
        });

        $('#dt-table').on('click', '.btn-hapus', function() {
            let id = $(this).data('id');
            Swal.fire({
                title: 'Are you sure?',
                text: "Data Akan Dihapus",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "post",
                        url: base_url + "data/deletedata",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        success: function(response) {
                            LoadTables();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: response.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    });
                }
            })
        });
    });
</script>