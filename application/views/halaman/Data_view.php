<div class="row mt">
    <div class="col-md-5">
        <div class="card">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">

                        <div class="col-sm-12">connectivity of tools <?= ($data->is_aktif == 'Y' ? '<span class="label label-success float-right">Running</span>' : '<span class="label label-danger float-right">Not Running</span>') ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-sm-3">Nama Pemilik</div>
                        <div class="col-sm-9"><?= $data->nama_pemilik ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-sm-3">Jenis Kendaraan</div>
                        <div class="col-sm-9"><?= $data->jenis_kendaraan ?> <?= ($data->tipe_langkah == null ? $data->tipe_langkah : '- ' . $data->tipe_langkah) ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-sm-3">No Plat</div>
                        <div class="col-sm-9"><?= $data->no_plat ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-sm-3">Tahun Produksi</div>
                        <div class="col-sm-9"><?= $data->tahun_produksi ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-sm-3">Data Pengujian</div>
                        <div class="col-sm-9">
                            <?php foreach ($uji as $key => $val): ?>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="pengujian_<?=$val->id_pengujian?>" name="pengujian" class="custom-control-input pengujian" <?=($val->aktif == "Y" ? 'checked':'')?> data-id="<?=$val->id_pengujian?>">
                                <label class="custom-control-label" for="pengujian_<?=$val->id_pengujian?>"><?=$val->nama_pengujian?></label>
                            </div>  
                        <?php endforeach ?>

                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="col-md-7">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-md-offset-1 box0">
            <div class="box1">
                <span class="">PPM</span>
                <h3 id="ppm">933</h3>
            </div>

        </div>
        <div class="col-md-6 col-sm-6 box0">
            <div class="box1">
                <span class="">CO</span>
                <h3 id="co">48%</h3>
            </div>

        </div>
    </div>
</div>
<div class="col-lg-4">
    <div class="card">
        <!-- <h4><i class="fa fa-angle-right"></i> Grafik Kadar Emisi PPM</h4> -->
        <div class="card-body text-center">
            <div id="line" style="height: 300px; margin: 0px auto;"></div>
        </div>
    </div>
</div>
<div class="col-lg-4">
    <div class="card">
        <div class="card-body text-center">
            <div id="line-co" style="height: 300px; margin: 0px auto;"></div>
        </div>
    </div>
</div>
<div class="col-lg-4">
    <div class="card p-2">
        <h4 class="font-we"> Tabel Data</h4>
        <div class="card-body">
            <table class="table" id="dt-table">
                <thead>
                    <tr>
                        <th>Date Time</th>
                        <th>PPM</th>
                        <th>CO %</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>12:01:02</td>
                        <td>150</td>
                        <td>70</td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
</div>

<script>
    $(function() {
$("#pengujian_1").change(function(){ // bind a function to the change event
        if( $(this).is(":checked") ){ // check if the radio is checked
            var id = $(this).data('id'); // retrieve the value
            var val = $(this).val(); // retrieve the value
            UpdateUji(val,id)

        }
    });

$("#pengujian_2").change(function(){ // bind a function to the change event
        if( $(this).is(":checked") ){ // check if the radio is checked
            var id = $(this).data('id'); // retrieve the value
            var val = $(this).val(); // retrieve the value
            UpdateUji(val,id) // retrieve the value
            
        }
    });

$("#pengujian_3").change(function(){ // bind a function to the change event
        if( $(this).is(":checked") ){ // check if the radio is checked
            var id = $(this).data('id'); // retrieve the value
            var val = $(this).val(); // retrieve the value
            UpdateUji(val,id) // retrieve the value

        }
    });

let table;
const id_ = '<?= $this->uri->segment(3) ?>';
let dps_ppm = [];
let dps_co = [];
        let dataLength = 15; // panjang data yang ditampilkan (horizontal), ditampilkan di bagian bawah grafik
        let updateInterval = 1500; //setiap 1,5 dtk data direfresh
        let xVal_ppm = 0;
        let yVal_ppm = 0;
        let xVal_co = 0;
        let yVal_co = 0;
        Loadstat();
        function Loadstat() {
            $.get(base_url + "api/data", {
                id_: id_
            },
            function(data, textStatus, jqXHR) {
                $('#ppm').text(data.data.ppm)
                $('#co').text(data.data.co + '%')
            },
            "json"
            );
        }
        
        table = $('#dt-table').DataTable({
            "dom": 'rtip',
            "scrollY": "200px",
            "scrollCollapse": true,
            "processing": false,
            "paging": false,
            ajax: {
                url: base_url + "api/data",
                type: "POST",
                data: function(data) {
                    data.id_ = id_;
                },
                dataSrc: "",
            },
            columns: [{
                data: "created"
            },
            {
                data: "ppm"
            },
            {
                data: "co"
            },

            ],

        });

        setInterval(function() {
            table.ajax.reload();
            Loadstat();
        }, 1000);

        var chartPPM = new CanvasJS.Chart("line", {

            title: {
                text: "Grafik Kadar Emisi PPM" 
            },
            
            data: [{
                type: "line", 
                lineColor:"green",
                dataPoints: dps_ppm
            }]
        })

        var chartCO = new CanvasJS.Chart("line-co", {
            title: {
                text: "Grafik Kadar Emisi CO %" 
            },
            data: [{
                type: "line", //tipe grafik yang digunakan, lihat di situsnya untuk lihat gaya lain
                lineColor:"red",
                dataPoints: dps_co //dps adalah data yang digunakan
            }]
        })

        var updateChartPpm = function(count) {
            $.getJSON(base_url + "api/chartppm?id_=" + id_, function(data) {
                var ppm = parseFloat(data.ppm) //mengambil data spesifik rate_float
                var co = parseFloat(data.co) //mengambil data spesifik rate_float

                yVal_ppm = ppm // mengisi variabel yVal dengan data usd
                yVal_co = co // mengisi variabel yVal dengan data usd
                count = count || 1;

                //melakukan perulangan data dengan for agar data dapat dijalankan
                for (var j = 0; j < count; j++) {
                    dps_ppm.push({
                        x: xVal_ppm,
                        y: yVal_ppm,
                        color:"red"
                    });
                    xVal_ppm++;

                    dps_co.push({
                        x: xVal_co,
                        y: yVal_co,
                        color:"blue"
                    });
                    xVal_co++;
                }

                if (dps_ppm.length > dataLength) {
                    dps_ppm.shift(); //maka hapus data awal dengan fungsi shift()
                }

                if (dps_co.length > dataLength) {
                    dps_co.shift(); //maka hapus data awal dengan fungsi shift()
                }

            })

            chartPPM.render();
            chartCO.render();
        };

        updateChartPpm(dataLength);

        setInterval(function() {
            updateChartPpm()
        }, updateInterval);

    });

function UpdateUji(val,id) {
    $.post(base_url+'data/updatepengujian', {val: val,id:id}, function(data, textStatus, xhr) {
        console.log(data);
        
    },"json");
    
}
</script>