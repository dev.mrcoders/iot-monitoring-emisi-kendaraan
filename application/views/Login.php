<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>LOGIN IOT CO</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="<?=base_url()?>assets/css/bootstrap.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <!--external css-->
    <link href="<?=base_url()?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/style-responsive.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@mdi/font@7.1.96/css/materialdesignicons.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	
		      <form class="form-login" action="#" id="form-login">
		        <h2 class="form-login-heading">sign in now</h2>
		        <div class="login-wrap">
		            <input type="text" class="form-control" placeholder="Username" autofocus name="username">
		            <br>
		            <input type="password" class="form-control" placeholder="Password" name="password">
		            <br>
		            <button class="btn btn-theme btn-block submit" href="index.html" type="submit"><i class="mdi mdi-lock"></i> SIGN IN</button>
		        </div>
		      </form>	  	
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?=base_url()?>assets/js/jquery.js"></script>
    <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("<?=base_url()?>assets/img/bg-2.jpg", {speed: 100});

        $(function () {
            $('form#form-login').submit(function (e) { 
                e.preventDefault();
                e.stopImmediatePropagation();
				let btn =$('.submit');

                $.ajax({
					type: "post",
					url: "<?=base_url()?>login/authchecking",
					data: $(this).serialize(),
					dataType: "json",
					beforeSend:function(){
						btn.html('<i class="mdi mdi-refresh"></i> Prosesss...');
						btn.attr('disabled');
					},
					success: function (response) {
						if(response.success){
							window.location.href='<?=base_url()?>'+response.data.redirect;
						}else{
							console.log(response.message);
						}
					},
                    complete: function() {
                        setTimeout(() => {
                            btn.html('<i class="mdi mdi-lock"></i> SIGN IN');
                            btn.attr('disabled', false);
                        }, 1000);

                    }
				});
            });

        });
    </script>


  </body>
</html>
