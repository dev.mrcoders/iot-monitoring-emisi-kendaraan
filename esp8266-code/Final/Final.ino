#include <WiFiManager.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>
#include <Arduino_JSON.h>
#include <MQUnifiedsensor.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>



#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

/************************Hardware Related Macros************************************/
#define         Board                   ("ESP8266")
#define         Pin                     (A0)  //Analog input 3 of your arduino
/***********************Software Related Macros************************************/
#define         Type                    ("MQ-9") //MQ9
#define         Voltage_Resolution      (3.3) // 3V3 <- IMPORTANT
#define         ADC_Bit_Resolution      (10) // For ESP8266
#define         RatioMQ9CleanAir        (9.6)
/*****************************Globals***********************************************/
MQUnifiedsensor MQ9(Board, Voltage_Resolution, ADC_Bit_Resolution, Pin, Type);
/*****************************Globals***********************************************/
/*****************************config server***********************************************/
#define AP_NAME "AP-DIMAS"
#define AP_PASS "12345678"

String serverName = "http://192.168.172.188/iot-monitoring-co/api/insert";
/*****************************Globals***********************************************/
void setup() {
  Serial.begin(115200);
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  display.display();
  delay(1000);

  WiFiManager wm;
//  wm.setSTAStaticIPConfig(IPAddress(192,168,0,99), IPAddress(192,168,0,1), IPAddress(255,255,255,0));
  bool res;
  res = wm.autoConnect(AP_NAME, AP_PASS); // password protected ap

  if (!res) {
    Serial.println("Failed to connect");
    wm.resetSettings();
    ESP.restart();
  }
  else {
    //if you get here you have connected to the WiFi
    Serial.println("connected...yeey :)");

  }
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  oleds(0, 15, "Wifi Has Connected");
  display.display();
  delay(1000);
  //Set math model to calculate the PPM concentration and the value of constants
  MQ9.setRegressionMethod(1); //_PPM =  a*ratio^b
  MQ9.init();
  Serial.print("Calibrating please wait.");
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  oleds(0, 15, "Calibrating please wait.");
  display.display();
  delay(1000);
  float calcR0 = 0;
  for (int i = 1; i <= 10; i ++)
  {
    MQ9.update(); // Update data, the arduino will read the voltage from the analog pin
    calcR0 += MQ9.calibrate(RatioMQ9CleanAir);
    Serial.print(".");
  }
  MQ9.setR0(calcR0 / 10);
  Serial.println("  done!.");
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  oleds(0, 15, "Done");
  display.display();
  delay(1000);


}

void loop() {
  MQ9.update();
  MQ9.setA(4269.6); MQ9.setB(-2.648);
  float CH4 = MQ9.readSensor();
  MQ9.setA(599.65); MQ9.setB(-2.244);
  float CO = MQ9.readSensor();
  float persenCo = (CO / 10000) * 100;
  
  SendToWebService(CH4, persenCo);
//  Serial.print(String("CH4 : ") + CH4);
//  Serial.print(String(" | CO : ") + CO);
//  Serial.println("");
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  oleds(0, 0, WiFi.localIP().toString());
  oleds(0, 15, String("HC : ") + CH4 + String(" PPM"));
  oleds(0, 30, String("CO : ") + persenCo + String(" %"));
  display.display();
  delay(500);


}

void oleds(int x, int y, String msg) {

  display.setCursor(x, y);
  display.println(msg);

}

void SendToWebService(float hc, float co) {
  //  if ((millis() - lastTime) > timerDelay) {
  //Check WiFi connection status
  if (WiFi.status() == WL_CONNECTED) {
    WiFiClient clients;
    HTTPClient http;
    String serverPath = serverName + "?ppm=" + (String) hc + "&co=" + (String) co;
    Serial.println(serverPath);
    Serial.print("[HTTP] begin...\n");
    if (http.begin(clients, serverPath)) {  // HTTP


      Serial.print("[HTTP] GET...\n");
      // start connection and send HTTP header
      int httpCode = http.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = http.getString();
          Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }

      http.end();
    } else {
      Serial.printf("[HTTP} Unable to connect\n");
    }


  }

  //  }
}
