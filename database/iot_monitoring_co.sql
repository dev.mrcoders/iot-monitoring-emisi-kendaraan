-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 13 Feb 2023 pada 18.24
-- Versi server: 5.7.33
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iot_monitoring_co`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(5) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(75) NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `username`, `password`, `created`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2023-02-02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_ambang_batas_emisi`
--

CREATE TABLE `tb_ambang_batas_emisi` (
  `id_emisi` int(5) NOT NULL,
  `id_jenis_kendaraan` int(5) NOT NULL,
  `deksripsi` varchar(25) NOT NULL,
  `co` varchar(15) NOT NULL,
  `hc` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_ambang_batas_emisi`
--

INSERT INTO `tb_ambang_batas_emisi` (`id_emisi`, `id_jenis_kendaraan`, `deksripsi`, `co`, `hc`) VALUES
(1, 1, '<', '3.0', '700'),
(2, 1, '>', '1.5', '200'),
(3, 2, '<', '4.5', '12000'),
(4, 2, '>', '4.5', '2000'),
(5, 3, '<', '5.5', '2400'),
(6, 3, '>', '4.5', '2000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_monitoring_emisi`
--

CREATE TABLE `tb_data_monitoring_emisi` (
  `id_monitoring_emisi` int(5) NOT NULL,
  `id_pengguna` int(5) NOT NULL,
  `ppm` varchar(15) NOT NULL,
  `co` varchar(15) NOT NULL,
  `id_pengujian` varchar(15) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_monitoring_emisi`
--

INSERT INTO `tb_data_monitoring_emisi` (`id_monitoring_emisi`, `id_pengguna`, `ppm`, `co`, `id_pengujian`, `created`) VALUES
(1, 3, '390', '25', '1', '2023-02-13 23:32:00'),
(2, 3, '345', '25', '2', '2023-02-13 23:32:00'),
(3, 3, '345', '25', '3', '2023-02-13 23:32:00'),
(4, 3, '555.24', '1.06', '3', '2023-02-13 23:58:00'),
(5, 3, '1557.92', '2.55', '3', '2023-02-13 23:58:00'),
(6, 3, '2421.20', '3.71', '3', '2023-02-13 23:58:00'),
(7, 3, '2793.54', '4.19', '3', '2023-02-13 23:58:00'),
(8, 3, '243.65', '0.53', '1', '2023-02-14 00:00:00'),
(9, 3, '642.93', '1.21', '1', '2023-02-14 00:00:00'),
(10, 3, '1139.42', '1.96', '1', '2023-02-14 00:00:00'),
(11, 3, '1359.67', '2.27', '1', '2023-02-14 00:00:00'),
(12, 3, '1050.06', '1.83', '1', '2023-02-14 00:00:00'),
(13, 3, '620.02', '1.17', '1', '2023-02-14 00:00:00'),
(14, 3, '275.77', '0.59', '1', '2023-02-14 00:00:00'),
(15, 3, '133.68', '0.32', '1', '2023-02-14 00:00:00'),
(16, 3, '151.58', '0.35', '2', '2023-02-14 00:00:00'),
(17, 3, '190.32', '0.43', '2', '2023-02-14 00:00:00'),
(18, 3, '530.16', '1.02', '2', '2023-02-14 00:00:00'),
(19, 3, '1177.77', '2.01', '2', '2023-02-14 00:00:00'),
(20, 3, '1928.60', '3.06', '2', '2023-02-14 00:00:00'),
(21, 3, '2634.07', '3.98', '2', '2023-02-14 00:00:00'),
(22, 3, '3142.88', '4.63', '2', '2023-02-14 00:00:00'),
(23, 3, '3012.67', '4.46', '2', '2023-02-14 00:00:00'),
(24, 3, '2359.64', '3.63', '2', '2023-02-14 00:00:00'),
(25, 3, '1302.77', '2.19', '2', '2023-02-14 00:00:00'),
(26, 3, '615.56', '1.16', '2', '2023-02-14 00:00:00'),
(27, 3, '308.93', '0.65', '2', '2023-02-14 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_data_pengguna`
--

CREATE TABLE `tb_data_pengguna` (
  `id_pengguna` int(5) NOT NULL,
  `nama_pemilik` varchar(45) NOT NULL,
  `id_kendaraan` int(5) NOT NULL,
  `no_plat` varchar(15) NOT NULL,
  `tahun_produksi` year(4) NOT NULL,
  `is_aktif` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_data_pengguna`
--

INSERT INTO `tb_data_pengguna` (`id_pengguna`, `nama_pemilik`, `id_kendaraan`, `no_plat`, `tahun_produksi`, `is_aktif`) VALUES
(2, 'tesss', 3, '34w34', 2012, 'N'),
(3, 'dsfsd', 1, 'sdfa3', 2006, 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenis_kendaraan`
--

CREATE TABLE `tb_jenis_kendaraan` (
  `id_kendaraan` int(5) NOT NULL,
  `jenis_kendaraan` varchar(25) NOT NULL,
  `bahan_bakar` varchar(25) DEFAULT NULL,
  `tipe_langkah` varchar(25) DEFAULT NULL,
  `tahun_produksi` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jenis_kendaraan`
--

INSERT INTO `tb_jenis_kendaraan` (`id_kendaraan`, `jenis_kendaraan`, `bahan_bakar`, `tipe_langkah`, `tahun_produksi`) VALUES
(1, 'Mobil', 'Bensin', NULL, 2007),
(2, 'Motor', NULL, '2', 2010),
(3, 'Motor', NULL, '4', 2010);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_list_pengujian`
--

CREATE TABLE `tb_list_pengujian` (
  `id_pengujian` int(5) NOT NULL,
  `nama_pengujian` varchar(15) NOT NULL,
  `aktif` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_list_pengujian`
--

INSERT INTO `tb_list_pengujian` (`id_pengujian`, `nama_pengujian`, `aktif`) VALUES
(1, 'Pengujian 1', 'N'),
(2, 'Pengujian 2', 'Y'),
(3, 'Pengujian 3', 'N');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `tb_ambang_batas_emisi`
--
ALTER TABLE `tb_ambang_batas_emisi`
  ADD PRIMARY KEY (`id_emisi`);

--
-- Indeks untuk tabel `tb_data_monitoring_emisi`
--
ALTER TABLE `tb_data_monitoring_emisi`
  ADD PRIMARY KEY (`id_monitoring_emisi`);

--
-- Indeks untuk tabel `tb_data_pengguna`
--
ALTER TABLE `tb_data_pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indeks untuk tabel `tb_jenis_kendaraan`
--
ALTER TABLE `tb_jenis_kendaraan`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indeks untuk tabel `tb_list_pengujian`
--
ALTER TABLE `tb_list_pengujian`
  ADD PRIMARY KEY (`id_pengujian`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_ambang_batas_emisi`
--
ALTER TABLE `tb_ambang_batas_emisi`
  MODIFY `id_emisi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_data_monitoring_emisi`
--
ALTER TABLE `tb_data_monitoring_emisi`
  MODIFY `id_monitoring_emisi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `tb_data_pengguna`
--
ALTER TABLE `tb_data_pengguna`
  MODIFY `id_pengguna` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_jenis_kendaraan`
--
ALTER TABLE `tb_jenis_kendaraan`
  MODIFY `id_kendaraan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_list_pengujian`
--
ALTER TABLE `tb_list_pengujian`
  MODIFY `id_pengujian` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
